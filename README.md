vtttool: Tooling for VirtualTabletop.io module development
==========================================================

This is tooling to help with developing modules for VirtualTabletop.io.

vtttool:

- Converts from YAML to JSON, allowing you to write in YAML which is less verbose.
- Automatically adds "id" keys to top level items, so you can omit them
- Checks for many possible errors in the JSON


Working in YAML
---------------

Here is part of the JSON for "Tutorial - Spinners":

```
{
  "9fhb": {
    "type": "button",
    "text": "DEFAULT",
    "clickRoutine": [
      {
        "func": "SELECT"
      },
      {
        "func": "SET",
        "property": "css",
        "value": null
      },
      {
        "func": "SELECT",
        "property": "id",
        "value": "8fr3",
        "mode": "set"
      },
      {
        "func": "SET",
        "property": "css",
        "value": "font-size: 60px"
      }
    ],
    "x": 217,
    "y": 278,
    "z": 7,
    "id": "9fhb"
  }
}
```

vtttool allows you to work in YAML, which looks like this:

```
9fhb:
  type: button
  text: DEFAULT
  # You can add YAML comments;
  # they'll be discarded when creating JSON.
  # You can also omit "id: 9fhb", it will
  # be automatically added.
  clickRoutine:
    - func: SELECT
    - func: SET
      property: css
      value: null
    - func: SELECT
      property: id
      value: 8fr3
      mode: set
    - func: SET
      property: css
      value: 'font-size: 60px'
  x: 217
  y: 278
  z: 7
```

To build the JSON which you'll need for the vtt package, you'll use:

```
vtttool xmpl.yaml > xmpl.json
```

JSON is valid YAML, so you can just rename your .json file to .yaml and start
using YAML features or convert portions. You could also use a tool like
[yq](https://github.com/mikefarah/yq) to convert to more terse YAML style:

```
yq eval -P xmpl.json> xmpl.yaml
```



Error Checking
--------------

vtttool automatically checks for many errors. For example, given a YAML file of:

```
9fhb:
  id: mismatched
  type: button
  text: DEFAULT
  clickRoutine:
    - func: SELECT
      id: id is not expected here
    - func: CALL
      widget: no-such-widget
      routine: no-such-routine
    - var ok = 'this is valid!'
    - this is not valid
    - func: IF
      condition: true
      thenRoutine: this should be a list, not a string
```

A call to `vtttool tiny-busted.yaml` reports:

```
tiny-busted.yaml: In 9fhb:
tiny-busted.yaml:2:3: warning: widget 9fhb disagrees with ID inside mismatched
     2 |   id: mismatched
       |   ^
tiny-busted.yaml: In 9fhb.clickRoutine.0.id:
tiny-busted.yaml:7:7: warning:  SELECT: property "id" is not known and will be ignored
     7 |       id: id is not expected here
       |       ^~
tiny-busted.yaml: In 9fhb.clickRoutine.1.widget:
tiny-busted.yaml:9:15: warning: CALL: widget refers to widget no-such-widget, but no such widget exists
     9 |       widget: no-such-widget
       |               ^~~~~~~~~~~~~~
tiny-busted.yaml: In 9fhb.clickRoutine.1.routine:
tiny-busted.yaml:10:16: warning: routine is named no-such-routine, but must end with "Routine" or be a ${variable}
    10 |       routine: no-such-routine
       |                ^~~~~~~~~~~~~~~
tiny-busted.yaml: In 9fhb.clickRoutine.3:
tiny-busted.yaml:12:7: warning: does not start with "var ": this is not valid
    12 |     - this is not valid
       |       ^
tiny-busted.yaml: In 9fhb.clickRoutine.4.thenRoutine:
tiny-busted.yaml:15:7: error: IF: expected thenRoutine to be an array but found a string
    15 |       thenRoutine: this should be a list, not a string
       |       ^
5 warnings
1 errors
```



License
-------

Part of vtttool: Tooling for developing modules for VirtualTabletop.io
Copyright (C) 2021  Alan De Smet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
