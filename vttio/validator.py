#! /usr/bin/python3

# Part of vtttool: Tooling for developing modules for VirtualTabletop.io
# Copyright (C) 2021  Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys
from vttio.yaml import yaml
import vttio.functions


def ajsontype(node):
    if isinstance(node, type):
        nodetype = node
    else:
        nodetype = type(node)

    # Need to test first, because subclass(bool,int) == True
    if nodetype == bool:
        if node == True: return 'true'
        if node == False: return 'false'
        return 'true or false'

    MAP = {
            dict: 'an object',
            list: 'an array',
            int: 'a number (integer)',
            float: 'a number (floating point)',
            str: 'a string',
            type(None): 'null',
            }

    for thetype, desc in MAP.items():
        if issubclass(nodetype, thetype):
            return desc
    return f"an unknown type ({type(node)})"

def human_list(list, conjunction="and"):
    """ Return english formatted list like "foo, bar, and qux"

    >>> from vttio.validator import human_list
    >>> human_list(["this"])
    'this'
    >>> human_list(["this", "that"], conjunction="or")
    'this or that'
    >>> human_list(["foo", "bar", "qux"])
    'foo, bar, and qux'
    """
    if len(list) == 1: 
        return list[0]
    if len(list) == 2:
        return f" {conjunction} ".join(list)
    return ", ".join(list[:-1])+f", {conjunction} "+list[-1]

def emit(msg):
    sys.stderr.write(msg)

def emitline(msg):
    emit(msg+"\n")


class ErrorFormatter:
    def __init__(self, use_color):
        self.last_report_path = None
        self.warnings = 0
        self.errors = 0
        if use_color == 'auto' or use_color is None or use_color == '':
            use_color = sys.stderr.isatty()
        self.use_color = use_color

        self.last_filename = None
        self.last_filebody = None

    def ansicode(self, code):
        if self.use_color:
            return f"\x1b[{code}"
        return ""
    def ansibold(self, body):
        return self.ansicode("1m")+body+self.ansicode("0m")
    def ansierror(self, body):
        return self.ansicode("01;31m")+body+self.ansicode("0m")
    def ansiwarning(self, body):
        return self.ansicode("01;35m")+body+self.ansicode("0m")

    def cfilepos(self, filename, line=None, column=None):
        out = f"{filename}:"
        if line is not None: out += f"{line}:"
        if column is not None: out += f"{column}:"
        return self.ansibold(out)

    def report(self, path, filename, line, column, level, msg, length=1):
        if path != self.last_report_path:
            self.last_report_path = path
            start = ".".join(path[:2])
            rest = ".".join(path[2:])
            if len(rest) > 0:
                rest = "."+rest
            emitline(f"{self.cfilepos(filename)} In {self.ansibold(start)}{rest}:")

        out = self.cfilepos(filename, line, column)
        levelfmt = lambda a,b: b
        if level == "warning": levelfmt = self.ansiwarning
        if level == "error": levelfmt = self.ansierror
        out += f" {levelfmt(level)}: {msg}"
        emitline(out)
        if line is not None:
            if filename != self.last_filename:
                self.last_filename = filename
                try:
                    self.last_filebody = open(filename).readlines()
                except: # Not much we can do
                    self.last_filebody = None
            if self.last_filebody is not None:
                noth=""
                content = self.last_filebody[line-1].rstrip()
                if column is not None:
                    c = column -1
                    start = content[:c]
                    mid = content[c:c+length]
                    end = content[c+length:]
                    content = start+levelfmt(mid)+end
                emitline(f" {line:5} | {content}")
                if column is not None:
                    highlight = '^'+("~"*(length-1))
                    caret = levelfmt(highlight)
                    emitline(f" {noth:5} | {' '*(column-1)}{caret}")


    def warning(self, path, filename, line, column, msg, length=1):
        self.report(path, filename, line, column, "warning", msg, length)
        self.warnings += 1

    def error(self, path, filename, line, column, msg, length=1):
        self.report(path, filename, line, column, "error", msg, length)
        self.errors += 1

    def final_report(self):
        if self.warnings > 0:
            emitline(f"{self.warnings} warnings")
        if self.errors > 0:
            emitline(f"{self.errors} errors")
            return False
        return True



class Path:
    def __init__(self, node, validator, parent, index):
        self.node = node
        self.validator = validator
        self.parent = parent
        self.index = index

    def __str__(self):
        return f"{self.describe()}: {self.node}"

    def position(self, value = False):
        lc = None
        if isinstance(self.node, list) or isinstance(self.node, dict):
            if hasattr(self.node, 'lc'):
                lc = self.node.lc
        elif hasattr(self.parent.node, 'lc'):
            if isinstance(self.index, str):
                if value:
                    lc = self.parent.node.lc.value(self.index)
                else:
                    lc = self.parent.node.lc.key(self.index)
            elif isinstance(self.index, int):
                lc = self.parent.node.lc.item(self.index)
            else:
                raise Exception("Unexpected")
        if lc is None:
            return None,None
        if isinstance(lc, tuple):
            line = lc[0]+1
            col = lc[1]+1
        else:
            line = lc.line+1
            col = lc.col+1
        return line,col

    def pathlist(self):
        result = []
        node = self
        while node.index is not None:
            result.insert(0, node.index)
            node = node.parent
        return [str(x) for x in result]

    def describe(self):
        ret = ".".join(self.pathlist())
        return ret

    def is_object(self):
        return isinstance(self.node, dict)

    def assert_type(self, types):
        return self.validator.assert_type(self, types)

    def child(self, index):
        if isinstance(index, str):
            if not self.assert_type(dict): return
            if index not in self.node:
                self.validator.warning(self, f"expected this to contain {index}, but it doesn't")
                return
            return Path(self.node[index], self.validator, self, index)
        if isinstance(index, int):
            if not self.assert_type(list): return
            if index < 0:
                raise Exception(f"Internal error: expecting a negative index into a list at {self.describe()}")
            if index > len(self.node):
                self.validator.warning(self, f"expected this to contain {index}, but the valid range is 0-{len(self.node)-1}")
                return
            return Path(self.node[index], self.validator, self, index)
        raise Exception(f"Index of {index} ({type(index)}) is neither a string nor an integer")

    def array_elements(self):
        if not self.assert_type(list):
                return []
        return [self.child(idx) for idx in range(0, len(self.node))]

    def object_values(self):
        if not self.assert_type(dict):
            return []
        return [self.child(idx) for idx in self.node]

    def find_local_property(self, property, default = None):
        if not isinstance(self.node, dict):
            self.validator.warning(self, f"expected {self.index} to be an object, but it's {ajsontype(self.node)}")
            return None
        if property in self.node:
            #sys.stderr.write(f"{property} IS in {self.describe()}\n")
            return self.node[property]
        #sys.stderr.write(f"{property} is NOT in {self.describe()}\n")
        return default

    def find_property(self, property, default = None):
        path = self
        while path is not None:
            result = path.find_local_property(property)
            if result is not None: return result
            nextwidget = path.find_local_property('inheritFrom')
            if nextwidget is None: return default
            path = self.validator.find_widget(nextwidget)
        return default

    def my_widget(self):
        if self.parent is None: return None
        if self.parent.parent is None: return None
        path = self
        while path.parent.parent is not None:
            path = path.parent
        return path


OP_ZERO = "random E LN10 LN2 LOG10E LOG2E PI SQRT1_2 SQRT".split()
OP_NUMBER_OR_STRING_ONE = "!".split()
OP_NUMBER_OR_STRING_TWO = "+ < <= == === != >= > && ||".split()
OP_NUMBERS_TWO = "- * ** / % atan2 hypot max min pow randInt toFixed".split()
OP_NUMBERS_ONE = "abs acos asin atan cbrt ceil cos exp floor log log10 log2 round sign sin sqrt tan trunc".split()
OP_NUMBERS_TWO_THREE = "randRange".split()
OP_STRING_ONE = "parseFloat toLocaleLowerCase to LocaleUpperCase toLowerCase toUpperCase trim trimEnd trimStart".split()
OP_STRING_TWO = "split endsWith localeCompare startsWith".split()
OP_STRING_THREE = "replace replaceAll".split()
OP_MISC_ONE = "from isArray".split()
OP_MISC_TWO = "concat padEnd padStart repeat charAt charCodeAt codePointAt includes join".split() 
OP_MISC_THREE = "slice substr".split()
OP_MISC_TWO_OR_THREE = "match search".split()
OP_ARRAY_OR_STRING_ONE = "length reverse sort numericSort pop shift".split()
OP_ARRAY_OR_STRING_TWO = "concatArray getIndex in indexOf lastIndexOf".split()
OP_VARMOD_MISC = "insert push remove unshift setIndex".split()

OP_SET_VALID = ["="] + OP_ZERO + OP_NUMBER_OR_STRING_ONE + OP_NUMBER_OR_STRING_TWO + OP_NUMBERS_TWO + OP_NUMBERS_ONE + OP_NUMBERS_TWO_THREE + OP_STRING_ONE + OP_STRING_TWO
OP_SET_DUBIOUS = OP_STRING_THREE
OP_ALL = OP_ZERO + OP_NUMBER_OR_STRING_ONE + OP_NUMBER_OR_STRING_TWO + OP_NUMBERS_TWO + OP_NUMBERS_ONE + OP_NUMBERS_TWO_THREE + OP_STRING_ONE + OP_STRING_TWO + OP_STRING_THREE + OP_MISC_ONE + OP_MISC_TWO + OP_MISC_THREE + OP_MISC_TWO_OR_THREE + OP_ARRAY_OR_STRING_ONE + OP_ARRAY_OR_STRING_TWO + OP_VARMOD_MISC

class Validator:
    def __init__(self, data, filename, warnings_are_errors, reporter):
        self.data = data
        self.filename = filename
        self.root = Path(self.data, self, None, None)
        self.last_report_path = None
        self.reporter = reporter
        if warnings_are_errors:
            self.warning = self.error


    def warning(self, path, msg, length=1, value=False):
        line,column = path.position(value=value)
        self.reporter.warning(path.pathlist(), self.filename, line, column, msg, length)

    def error(self, path, msg, length=1, value=False):
        line,column = path.position(value=value)
        self.reporter.error(path.pathlist(), self.filename, line, column, msg, length)

    def find_widget(self, widgetid):
        if widgetid in self.root.node:
            return self.root.child(widgetid)
        return None

    def validate_meta(self, path):
        if not self.assert_type(path, dict, 'meta information'): return
        info = path.node.get('info')
        if info is None:
            self.warning(path, 'lacks an info dictionary.')

    def assert_type(self, path, types, description="this", context=None):
        if isinstance(types, type):
            types = [types]
        if isinstance(path.node, str) and "${" in path.node:
            # Cannot validate runtime calculated values
            return True
        for atype in types:
            if isinstance(path.node, atype):
                return True
        descs = [ajsontype(x) for x in types]
        prefix = ""
        if context is not None: prefix = context+": "
        if len(types) > 1:
            self.error(path, f'{prefix}expected {description} to be one of {human_list(descs, "or")} but found {ajsontype(path.node)}')
        else:
            self.error(path, f'{prefix}expected {description} to be {descs[0]} but found {ajsontype(path.node)}')
        return False

    def assert_required_child_type(self, path, name, types, description="this"):
        if name not in path.node:
            self.warning(path, f"{description} lacks required {name}")
            return False
        return self.assert_type(path.child(name), types, description=description)


    def assert_in_options(self, path, options, description, descriptions = None, context=None):
        if isinstance(path.node, str) and "${" in path.node: # Calculated at runtime; we cannot know
            return True
        if descriptions is None:
            descriptions = description+"s"
        prefix = ""
        if context is not None: prefix = context+": "
        if path.node not in options:
            self.error(path, f"{prefix}unknown {description} {path.node} (valid {descriptions} are: {human_list(options, conjunction='or')})")
            return False
        return True

    def assert_refers_to_widget(self, path, context = None):
        thelist = path.node
        if isinstance(path.node, list):
            thelist = path.array_elements()
        else:
            thelist = [path]

        all_found = True
        for child in thelist:
            if isinstance(child.node, str) and "${" in path.node:
                # We can't really test, so assume it's okay
                return True

            if self.find_widget(child.node) is None:
                prefix = ""
                if context is not None: prefix = context + ": "
                value = child == path
                self.warning(child, f"{prefix}{path.index} refers to widget {child.node}, but no such widget exists", length=len(str(child.node)), value=value)
                all_found = False

        return all_found

    def validate_function_obj_CALL(self, path):
        routinepath = path.child('routine')
        routine = routinepath.node
        if not (routine.endswith("Routine") or routine.endswith('}')):
            self.warning(routinepath, f'routine is named {routine}, but must end with "Routine" or be a ${{variable}}', value=True, length=len(routine))
        widgetid = path.node.get('widget')
        widget = None
        if widgetid is None:
            widget = path.my_widget()
        if widget is not None and "${" not in routine:
            prop = widget.find_property(routine)
            if prop is None:
                self.warning(routinepath, f"tried to CALL {routine} in {widget.index}, but it doesn't exist", value=True, length=len(routine))

    def validate_function_obj_IF(self, path):
        if 'condition' not in path.node and 'operand1' not in path.node:
            self.warning(path, "lacks either condition or operand1; one of them is required")
        if 'thenRoutine' not in path.node and 'elseRoutine' not in path.node:
            self.warning(path, 'IF has neither thenRoutine nor elseRoutine, and so will never run a routine', length=4)

    def validate_function_obj_FLIP(self, path):
        if 'count' in path.node and isinstance(path.node, int):
            if path.node['count'] < 0:
                self.warning(path.child('count'), f"negative counts like {path.node['count']} are nonsensical")


    def validate_function_obj_SELECT(self, path):
        if path.node.get('relation', None) == 'in':
            if not isinstance(path.node.get('value', None),list):
                if 'value' in path.node:
                    descvalue = ajsontype(path.node['value'])
                else:
                    descvalue = 'not present'
                self.warning(path, f'when "relation" is "in", "value" must be a list, but it is {descvalue}')
        maxval = path.node.get('max',0)
        if isinstance(maxval, int) and maxval < 0:
            self.warning(path, f"max is {maxval}, but a positive value is expected")
        if 'sortBy' in path.node:
            subpath = path.child('sortBy')
            if isinstance(subpath, dict):
                pass
                # TODO: re-implement
                #validate_function_obj_helper(subpath, required={"key": str}, optional={"reverse": bool})

    def validate_function_obj_SET(self, path):
        if 'relation' in path.node:
            relation = path.node['relation']
            if relation == '==':
                self.warning(path, '"relation" of "==" will be treated as "="')
            if relation not in OP_ALL:
                self.error(path, f'"relation" must be a valid compute operation, but is "{relation}".')

    def validate_function_obj_TIMER(self, path):
        my_widget = path.my_widget()
        precision = my_widget.find_property('precision', 1000)
        mode = path.node.get('mode', 'toggle')

        if mode in "pause start toggle reset".split():
            for unused in ['value', 'seconds']:
                if unused in path.node:
                    self.warning(path.child(unused), f"timer's mode is {mode}, so \"{unused}\" is ignored")
        else:
            ms = 0
            subpath = path

            if 'value' in path.node:
                subpath = path.child('value')
                if isinstance(subpath.node, str) and "${" not in subpath.node:
                    prop = my_widget.find_property(subpath.node)
                    if prop is None:
                        self.warning(subpath, f"refers to property {subpath.node}, but there is no such property in this widget or any widget that it inherits from")
                else:
                    ms = subpath.node
            elif 'seconds' in path.node:
                subpath = path.child('seconds')
                ms = subpath.node * 1000

            if ms < 0:
                self.warning(subpath, f"regative value of {ms} is probably nonsensical")
            if ms == 0:
                if subpath is path:
                    self.warning(subpath, "default value of 0 for milliseconds is probably nonsensical")
                else:
                    self.warning(subpath, "0 value is probably nonsensical")
            elif subpath.node < precision:
                self.warning(subpath, f"timer's precision is {precision} ms; \"{subpath.index}\" of {ms} ms may not be meaningful")


    SPECIAL_FUNCTIONS = {
        'CALL': validate_function_obj_CALL,
        'FLIP': validate_function_obj_FLIP,
        'IF': validate_function_obj_IF,
        'SELECT': validate_function_obj_SELECT,
        'SET': validate_function_obj_SET,
        'TIMER': validate_function_obj_TIMER,
    }


    def validate_function_str(self, path):
        if not isinstance(path.node, str):
            raise Exception(f"Internal error: validate_function_str called with non-string: {type(path.node)}")
        if not path.node.startswith('var '):
            self.warning(path, f"does not start with \"var \": {path.node}")

    def validate_function_obj(self, path):
        if not path.is_object():
            raise Exception(f"Internal error: validate_function_obj called with non-object: {type(path.node)}")
        if "func" not in path.node:
            self.error(path, 'function object lacks a "func"')
            return False
        func = path.node["func"]

        if func not in vttio.functions.FUNCTIONS:
            more = ''
            if func.upper() in vttio.functions.FUNCTIONS:
                more = f'; did you mean {func.upper()}?'
            self.error(path, f"func={func} is not a known function{more}")
            return False

        funcrules = vttio.functions.FUNCTIONS[func]
        if funcrules.TODO: return # We can't do anything with it.
        expected_props = set(funcrules.properties)
        found_props = set(path.node)

        # Look for stuff that shouldn't be there
        unexpected_props = found_props - expected_props
        for unexpected in sorted(unexpected_props):
            self.warning(path.child(unexpected), f' {func}: property "{unexpected}" is not known and will be ignored', length=len(unexpected))

        # Look for stuff that is required, but missing
        required_props = set([prop for prop in expected_props if funcrules.properties[prop].is_required])
        missing_props = required_props - found_props
        if len(missing_props) > 0:
            for prop in sorted(missing_props):
                self.error(path, f'{func}: property "{prop}" is required, but is not present', value=True, length=len(func))
            return False

        # Check what we do have
        bad_type = False
        good_props = found_props & expected_props
        for prop in sorted(good_props):
            subpath = path.child(prop)
            rules = funcrules.properties[prop]
            if not self.assert_type(subpath, rules.types, description=prop, context=func): 
                bad_type = True
                continue

            hits = []
            for dislike in sorted(rules.exclusive_of):
                if dislike in path.node:
                    hits.append(dislike)
            if len(hits)>0:
                self.warning(path, f"{func}: {human_list([prop]+hits)} were specified, but are mutually exclusive; only {prop} will be used")

            if rules.is_widget: self.assert_refers_to_widget(subpath, context=func)
            if rules.options: self.assert_in_options(subpath, rules.options, prop, context=func)
            if rules.min is not None and isinstance(subpath.node, type(rules.min)) and (subpath.node < rules.min):
                self.warning(subpath, f"{func}: {prop} must be at least {rules.min}, but is {subpath.node}")
            if rules.is_code: self.validate_routine(subpath)

        # We can't continue, as we promise the SPECIAL_FUNCTIONS that values are of the correct type
        if bad_type: return False

        # Special case checks
        if func in self.SPECIAL_FUNCTIONS:
            self.SPECIAL_FUNCTIONS[func](self, path)

        pass


    def validate_function(self, path):
        if isinstance(path.node, str):
            self.validate_function_str(path)
        elif path.is_object:
            self.validate_function_obj(path)
        else:
            self.warning(path, f'expected function to be a string or object, but it is {ajsontype(path.node)}')

    def validate_routine(self, path):
        if not self.assert_type(path, list, 'routine'): return
        for child in path.array_elements():
            self.validate_function(child)

    def validate_widget_deck(self, path):
        self.assert_required_child_type(path, 'cardTypes', dict, description="deck")
        self.assert_required_child_type(path, 'faceTemplates', list, description="deck")

    def validate_widget_card(self, path):
        okay = True
        okay &= self.assert_required_child_type(path, 'deck', str, description="card")
        okay &= self.assert_required_child_type(path, 'cardType', str, description="card")
        if not okay: return False

        deckpath = path.child('deck')
        typepath = path.child('cardType')
        if not self.assert_refers_to_widget(deckpath): 
            return False

        deck = self.find_widget(deckpath.node)
        if 'cardTypes' not in deck.node:
            self.warning(path, f"card expected deck {deckpath.node} to contain cardType {typepath.node}, but the deck lacks a cardTypes property entirely")
            return False
        cardtypes = deck.child('cardTypes')
        prop = cardtypes.find_property(typepath.node)
        if prop is None:
            self.warning(path, f"card expected deck {deckpath.node} to contain cardType {typepath.node}, but it doesn't")
        return True

    SPECIAL_WIDGETS = {
        'deck': validate_widget_deck,
        'card': validate_widget_card,
        }

    WIDGET_TYPES = ["button", "canvas", "card", "deck", "holder", "label", "pile", "spinner", "timer"]
    #(no capital letters allowed). If no value (or any other value) is given, it will become a _BasicWidget_. |

    def validate_widget(self, path):
        if not self.assert_type(path, dict, description="widget"):
            return

        if 'id' in path.node and path.node['id'] != path.index:
            # It's okay if it's missing (we'll add it), but it's not okay for it to
            # disagree.
            self.warning(path, f'widget {path.index} disagrees with ID inside {path.node["id"]}')
        if 'parent' in path.node:
            subpath = path.child('parent')
            if self.assert_type(subpath, str):
                self.assert_refers_to_widget(subpath)
        if 'inheritFrom' in path.node:
            subpath = path.child('inheritFrom')
            if self.assert_type(subpath, [str, dict]):
                if isinstance(subpath.node, str):
                    # TODO might be "*"? If so, inherit from everyone!?
                    self.assert_refers_to_widget(subpath)
                else:
                    # TODO: If it's a dictionary, the keys are widget names, and the values are lists of function names to inherit
                    pass


        if 'type' in path.node:
            subpath = path.child('type')
            if self.assert_type(subpath, str):
                nodetype = subpath.node
                if nodetype in self.SPECIAL_WIDGETS:
                    self.SPECIAL_WIDGETS[nodetype](self, path)
                if nodetype not in self.WIDGET_TYPES:
                    self.error(path, f"unknown widget type {nodetype}; will become a BasicWidget (valid widget types are: {human_list(self.WIDGET_TYPES)})")




        for child in path.object_values():
            if child.index.endswith('Routine'):
                self.validate_routine(child)


    def validate(self):
        for subpath in self.root.object_values():
            if subpath.index == "_meta":
                self.validate_meta(subpath)
            else:
                self.validate_widget(subpath)

def validate(data, filename, warnings_are_errors):
    reporter = ErrorFormatter(use_color="auto")
    v = Validator(data, filename, warnings_are_errors, reporter)
    v.validate()
    return reporter.final_report()
