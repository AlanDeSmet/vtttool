#! /usr/bin/python3

# Part of vtttool: Tooling for developing modules for VirtualTabletop.io
# Copyright (C) 2021  Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys

import ruamel.yaml
yaml=ruamel.yaml.YAML(typ='rt', pure=True)
yaml.indent(mapping=2, sequence=4, offset=2)
yaml.safe_load = yaml.load
yaml.YAMLError = ruamel.yaml.error.YAMLError

#except:
#    import yaml
#    sys.stderr.write("ruamel.yaml is not available; using PyYAML\n")
