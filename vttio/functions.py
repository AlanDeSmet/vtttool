# Part of vtttool: Tooling for developing modules for VirtualTabletop.io
# Copyright (C) 2021  Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


import os
from vttio.yaml import yaml

FUNCTIONS = {}

def js_type_to_py(js_type):
    if js_type == 'any': return object
    if js_type == 'String': return str
    if js_type == 'Boolean': return bool
    if js_type == 'Object': return dict
    if js_type == 'code': return list
    if js_type == 'Array': return list
    if js_type == 'Number': return float
    if js_type == 'uint': return int
    raise Exception(f"Unknown JavaScript(ish) type: {js_type}")

class Property:
    def __init__(self, name, obj):
        self.name = name

        if isinstance(obj, str):
            obj = { 'type': obj }
        
        thetype = obj.get('type', 'String')
        if isinstance(thetype, str):
            thetype = [thetype]
        self.types = [js_type_to_py(x) for x in thetype]
        if "Number" in thetype: self.types.append(int)
        self.is_code = len(thetype) == 1 and thetype[0] == "code"
        self.min = 0 if len(thetype) == 1 and thetype[0] == 'uint' else None

        self.is_required = obj.get('required', False)
        self.is_widget = obj.get('is_widget', False)
        self.exclusive_of = obj.get('exclusive', [])
        if isinstance(self.exclusive_of, str):
            self.exclusive_of = [self.exclusive_of]
        self.options = obj.get('options', None)

        allowed = set("type required is_widget exclusive options".split())
        found = set(obj)
        unexpected = found - allowed

        if len(unexpected) > 0:
            raise Exception(f"Found unknown property settings: {unexpected}")

    def __str__(self):
        return f"{self.name} {self.types}, required? {self.is_required}, min {self.min}, code? {self.is_code}, widget? {self.is_widget}, exclusive? {self.exclusive_of}, options? {self.options}"


class Function:
    def __init__(self, name, obj):
        self.name = name
        self.properties = {}
        self.properties["func"] = Property("func", {
            'required': True,
            'options': [ name ],
            })
        for comment in ["comment", "note"]:
            self.properties[comment] = Property(comment, { 'type': 'any' })
        if obj == "TODO":
            self.TODO = True
        else:
            self.TODO = False
            for prop, val in obj.items():
                self.properties[prop] = Property(prop, val)
                #print(self.properties[prop])

def initialize():
    LIBDIR = os.path.dirname(os.path.realpath(__file__))
    FUNCTIONS_FILE = LIBDIR+"/functions-data.yaml"
    with open(FUNCTIONS_FILE) as f:
        functions = yaml.safe_load(f)
        for name, body in functions.items():
            global FUNCTIONS
            FUNCTIONS[name] = Function(name, body)

initialize()

