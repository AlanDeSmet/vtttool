# Part of vtttool: Tooling for developing modules for VirtualTabletop.io
# Copyright (C) 2021  Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import unittest
from unittest import TestCase

import vttio.validator

def coll
vttio.validator.emit = 


class FunctionCALL(TestCase):


class ArbitraryNameUsedInMessages(unittest.TestCase):

    def setUp(self):
        pass # Called before every test_ function

    def tearDown(self):
        pass # Called after every test_ function (provided setUp succeeded)

    def test_equality(self):
        """ Some simple equality tests

        The first line in this docstring will be used in output"""
        self.assertEqual(1,1)
        self.assertNotEqual(1,2, "One and Two are the same")

        self.assertAlmostEqual(3.14, 3.13, places=1)
        self.assertNotAlmostEqual(3.14, 3.13, places=2)

        self.assertGreater(3,2)
        self.assertGreaterEqual(2,2)
        self.assertLess(2,3)
        self.assertLessEqual(2,2)

    def test_several_assertions(self):
        self.assertTrue(True)
        self.assertFalse(False)

        a = {1:2}
        b = a
        c = {1:2}
        self.assertIs(a,b)
        self.assertIsNot(a,c)

        self.assertIsNone(None)
        self.assertIsNotNone("abc")

        self.assertIn("b", "abc")
        self.assertNotIn("z", "abc")

        self.assertIsInstance(2,int)
        self.assertNotIsInstance(2,str)

        self.assertRegex("random text", r't.x')
        self.assertNotRegex("random text", r't.z')

        self.assertCountEqual([1,2],[2,1])

        self.assertMultiLineEqual("a\nb","a\nb")

        self.assertSequenceEqual([1,2],(1,2))
        # The following are automatically used by assertEqual if the data types
        # are appropriate
        self.assertListEqual([1,2],[1,2])
        self.assertTupleEqual((1,2),(1,2))
        self.assertSetEqual(set([1,2]),set([2,1]))
        self.assertDictEqual({1:2,3:4},{3:4,1:2})

        if False:
            self.fail("everything is terible")



        with self.assertRaises(TypeError):
            open()

def example_function(a,b):
    """ Return sum of a and b

    >>> example_function(1,2)
    3
    >>> example_function(None,None)
    Traceback (most recent call last):
    ...
    TypeError: None is not valid
    """
    if a is None:
        raise TypeError("None is not valid")
    return a+b

def load_tests(loader, tests, ignore):
    """ Ensure unittest runs doctest """
    import doctest
    tests.addTests(doctest.DocTestSuite())
    return tests


# In a primary file, use something like
#     if args.run_tests:
#         unittest.main()
#
# For a library, use something like:
#    if __name__ == '__main__':
#        unittest.main()
#
# You can just test doctests with:
#    if do_doctests:
#        import doctest
#        doctest.testmod()





if __name__ == '__main__':
    unittest.main()
